# MakeThrough
## Introduction
*MakeThrough* est une classe PHP écrite de A à Z en socket. Son but est de faciliter la proxification et l'envoi de requêtes et plus spécialement l'envoi de requêtes sur de gros fichiers.  
## Installation  
Placer `MakeThrough.class.php` dans le répertoire de votre choix, et incluez-le. Aucun module PHP n'est nécessaire à son fonctionnement.  
### Documentation  
Pour apprendre comment se servir de MakeThrough, vous pouvez vous référer au wiki:
 - [Présentation](../../wikis/home)
 - [Les méthodes](../../wikis/methodes)
 - [Les variables](../../wikis/variables)