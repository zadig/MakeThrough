<?php
/**
 * @file  : MakeThrough.class.php
 * @author: Thomas, thomas [at] botch [dot] io
 * @copyright: YES or NO, don't care. :)
 * @date  : July 2015
 *
 * class MakeThrough
 *
 * Arguments:
 *  - string $url    l'URL à traiter
 *
 *
 * DOCUMENTATION
 *
 *  --------------------------------------------------------------------
 * private $opts : cette variable va renseigner l'ensemble des paramètres
 *                 à prendre en compte lors de la requête.
 *
 *
 * [debug]        : active le debug
 * [ip]           : ip du serveur distant
 * [port]         : port à utiliser. Par défaut 80, passe en 443 si SSL détecté.
 * [method]       : GET, POST, OPTIONS etc etc. Par défaut: GET
 * [headers]      : tableau associatif réunissant les headers à envoyer au serveur distant
 * [bufferLength] : taille du buffer (utilisé avec fgets()). Par défaut: 1024
 * [path]         : path de la requête (/foo/bar/file.rar)
 * [countBytesTransfered] : Compte le nombre de Bytes transférés (dans les deux sens)
 *                          si TRUE, sinon rien. Par défaut: TRUE
 * [supportRange] : prendre en compte le range (à spécifier). Par défaut: TRUE
 *
 * --------------------------------------------------------------------
 * private $urlinfo: issue de parse_url.
 *
 * --------------------------------------------------------------------
 * private $socket : le socket
 * private $socketError: chaîne de l'erreur
 * private $socketErrno: numéro de l'erreur
 *
 * --------------------------------------------------------------------
 * public $request : variable qui stocke l'ensemble des données modifiées/ajoutées/créées
 *                   lors de la requête
 *
 * [client] : côté client :
 *  - [headers]       : les headers à envoyer, RAW
 *  - [bytesSent]     : quantité de Bytes envoyés
 *  - [bytesReceived] : quantité de Bytes reçus
 * [server] : côté serveur :
 *  - [headers]       : les headers reçus, RAW
 *  - [parsedHeaders] : les headers en tableau associatif
 *  - [httpCode]      : le code HTTP de la réponse
 *  - [httpMsg]       : le message HTTP associé au code de la réponse
 *
 * --------------------------------------------------------------------
 * void __construct(string $url) :
 *  function d'initialisation : on lui envoie l'URL, et la fonction va :
 *  - parser l'url pour en extraire les informations
 *  - déterminer les options 'vitales' de la requête (SSL)
 *  - remplir les headers
 *
 * void setOption(string $name, string $value) :
 *  function permettant de modifier des options :
 *   - debug (bool) : activer/désactiver le debug
 *   - random-agent (bool) : set un user-agent random, sinon NULL
 *   - host (string) : modifie l'host, en conséquent l'IP
 *   - header (string array) : ajoute/modifie l'header array[0] avec la valeur array[1]
 *   - method (string) : modifie la méthode
 *   - path (string) : modifie le path (parse automatiquement)
 *   - query (string) : modifie la query
 *   - countbytestransfered (bool) : active/désactive le comptage des bytes transférés
 *   - supportrange (bool) : active/désactive le support des ranges
 *   - bufferlength (int) : modifie la taille du buffer socket
 *   - ssl (bool) : active/désactive SSL
 *
 * void encodePath(string $value) :
 *  function qui encode le path correctement
 *
 * void requestServerHeaders() :
 *  function qui va initialiser la connexion, et envoyer les headers préalablement instaurés.
 *  cette fonction va :
 *   - automatiquement initialiser le socket
 *   - automatiquement envoyer les headers
 *   - automatiquement recevoir et parser les headers du serveur
 *
 * void parseServerHeaders() :
 *  function qui va parser en un tableau associatif les headers reçus.
 *
 * void requestServerContent(bool $whole = TRUE, function $callback = NULL) :
 *  sûrement la fonction la plus intéressante.
 *  Cette fonction va récupérer le contenu de la page (à condition que requestServerHeaders()
 *  fu exécuté).
 *  Le paramètre $whole spécifie la manière dont on veut recevoir les données :
 *    - TRUE  : on va recevoir tout d'un coup, sans traitement pendant la réception.
 *    - FALSE : on va recevoir $opts['bufferLength'] bytes, et on va appeler la fonction $callback SI différente de null.
 *              la fonction $callback doit comporter un argument : $raw. Ce sont les bytes téléchargés.
 *
 *  Dans le premier cas, le comptage se fait à la fin, dans le second cas, le comptage se fait dans la boucle de réception.
 *
 *  EXAMPLE :
 *
 *  $MT = new MakeThrough(http://example.com/file.7z);
 *  $MT->setOption('debug', TRUE);
 *  $MT->setOption('header',array("Cookie","a=1,b=false));
 *  $MT->setOption('bufferLength', 4096);
 *  $MT->requestServerHeaders();
 *  $filesize = $MT->request['server']['parsedHeaders']['content-length'];
 *  $MT->requestServerContent(FALSE, echo);
 *
 **/

class MakeThrough {

  /**
   *
   * @var client
   *
   **/

  private $urlinfo = NULL;

  /**
   *
   * @var server
   *
   **/
  public $opts = array(
    'debug'                 =>  FALSE,
    'ip'                    =>  NULL,
    'port'                  =>  80,
    'method'                =>  'GET',
    'headers'               =>  array('Host'            =>  NULL,
                                      'Connection'      =>  'close',
                                      'Accept'          =>  'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                                      'User-Agent'      =>  NULL,
                                      'DNT'             =>  '1',
                                      'Accept-Encoding' =>  'gzip, deflate, sdch',
                                      'Accept-Language' =>  'fr-FR,fr;q=0.8,en-US;q=0.6,en;q=0.4',
                            ),
    'bufferLength'          => 1024,
    'path'                  => NULL,
    'countBytesTransfered'  => TRUE,
    'supportRange'          => TRUE );

  private $socket      = NULL;
  private $socketError = NULL;
  private $socketErrNo = NULL;

  public $request = array(
    'client'  =>  array(  'headers'       =>  NULL,
                          'bytesSent'     =>  0,
                          'bytesReceived' => 0),

    'server'  =>  array(  'headers'       =>  NULL,
                          'parsedHeaders' =>  array(),
                          'content'       =>  NULL,
                          'httpCode'      =>  0,
                          'httpMsg'       =>  NULL));



  public function __construct($url) {

    /* On parse l'URL */
    $this->urlinfo = parse_url($url);

    /* On active (ou pas) certaines options */
    $this->opts['ssl'] = (strtolower($this->urlinfo['scheme'])=='https');

    /* On remplit les headers */
    $this->setOption('Host',$this->urlinfo['host']);
    if (array_key_exists('port',$this->urlinfo)) {
      $this->setOption('headers',array('Host',$this->urlinfo['host'].':'.$this->urlinfo['port']));
      $this->opts['port'] = intval($this->urlinfo['port']);
    } else {
      $this->opts['port'] = $this->opts['ssl']?443:80;
    }
    $this->setOption('path',$this->urlinfo['path']);
    if (array_key_exists('query',$this->urlinfo)) {
      $this->setOption('query',$this->urlinfo['query']);
    }

  }

  public function setOption($name, $value) {
    $name = strtolower($name);
    switch ($name) {

      case 'debug':
        $this->opts['debug'] = $value;
        break;

      case 'random-agent':
        if ($value == TRUE) {
          $browsers = json_decode(file_get_contents(__DIR__.'browsers.json'),TRUE);
          $this->opts['headers']['User-Agent'] = $browsers[rand(0,count($browsers)-1)];
        } else {
          $this->opts['headers']['User-Agent'] = NULL;
        }
        break;

      case 'host':
        $this->opts['headers']['Host'] = $value;
        $this->opts['ip'] = gethostbyname($value);
        break;

      case 'header':
        list($name, $value) = $value;
        $this->opts['headers'][$name] = $value;
        break;

      case 'method':
        $this->opts['method'] = $value;
        break;

      case 'path':
        $this->opts['path'] = $this->encodePath($value);
        break;

      case 'query':
        $this->opts['path'] .= '?'.$value;
        break;

      case 'countbytestransfered':
        $this->opts['countBytesTransfered'] = $value;
        break;

      case 'supportrange':
        $this->opts['supportRange'] = $value;
        break;

      case 'bufferlength':
        $this->opts['bufferlength'] = intval($value);
        break;

      case 'ssl':
        $this->opts['ssl'] = $value;
        break;

      default:
        break;
    }
  }

  private function encodePath($value) {
    return preg_replace('/%2F/','/',rawurlencode($value));
  }

  public function requestServerHeaders() {
    if ($this->opts['ip'] == NULL OR $this->opts['headers']['Host'] == NULL ) Throw new Exception('No Host/IP specified.');

    /* Création des headers */
    foreach($this->opts['headers'] as $name=>$value) {
      $this->request['client']['headers'] .= $name . ': ' . $value . "\r\n";
    }
    $this->request['client']['headers'] = strtoupper($this->opts['method']) . ' ' . $this->opts['path'] . " HTTP/1.1\r\n" . $this->request['client']['headers'] . "\r\n";

    /* Création du socket */
    $remoteSocket = ($this->opts['ssl'])?'ssl://'.$this->urlinfo['host'].':'.$this->opts['port']:$this->opts['ip'].':'.$this->opts['port'];
    $this->debug('Connecting to the server ...');
    $this->socket = stream_socket_client($remoteSocket, $this->socketErrno, $this->socketError);
    stream_set_timeout($this->socket,20000);
    set_time_limit(0);
    if (!$this->socket) Throw new Exception('Unable to connect to '.$this->opts['ip'].':'.$this->opts['port']);
    $this->debug($this->request['client']['headers']);

    /* Envoi des headers */
    fputs($this->socket,$this->request['client']['headers']);
    if ($this->opts['countBytesTransfered']) $this->request['client']['bytesSent'] += strlen($this->request['client']['headers']); // Log du traffic

    /* Réception des headers */
    while ($raw = fgets($this->socket, $this->opts['bufferLength']) and $raw != "\r\n") {
      $this->request['server']['headers'] .= $raw;
    }
    if ($this->opts['countBytesTransfered']) $this->request['client']['bytesReceived'] += strlen($this->request['server']['headers']); // Log du traffic
    $this->parseServerHeaders();
  }

  private function parseServerHeaders() {
    $headers = array_filter(explode("\r\n",$this->request['server']['headers']));

    /* Récupération du code HTTP */
    if (!preg_match('/^HTTP\/1\.1 ([0-9]{2,5}) ([a-zA-Z0-9\s_-]+)?$/', $headers[0], $matches)) {
      Throw new Exception('The headers sent by the server are not understandable.');
    }
    $this->request['server']['httpCode'] = intval($matches[1]);
    $this->request['server']['httpMsg']  = $matches[2];

    /* On parse les headers */
    foreach($headers as $h) {
      if (preg_match('/^([^:]+):(?:[\s]+)?(.+)$/', $h, $matches)) {
        $this->request['server']['parsedHeaders'][strtolower($matches[1])] = $matches[2];
      }
    }
    $this->debug('HTTP Code : '. $this->request['server']['httpCode']);
    $this->debug('HTTP Msg : ' . $this->request['server']['httpMsg']);
  }

  private function debug($str) {
    if ($this->opts['debug']) file_put_contents('php://stderr','<debug> '.$str."\r\n");
  }

  public function requestServerContent($whole = True, $callback = NULL) {
    ini_set('memory_limit', -1);
    if ($this->socket == FALSE OR $this->socket == NULL) {
      Throw new Exception('No connection with the server made yet.');
    }

    /* On récupère tous d'un coup */
    if ($whole) {
      while ($raw = fgets($this->socket, $this->opts['bufferLength'])) {
        $this->request['server']['content'] .= $raw;
        if (feof($this->socket)) break;
      }
      if ($this->opts['countBytesTransfered']) $this->request['client']['bytesReceived'] += strlen($this->request['server']['content']); // Log du traffic
    } else { /* On récupère pas tous d'un coup. x) */
      while ($raw = fgets($this->socket, $this->opts['bufferLength'])) {
        $this->request['server']['content'] .= $raw;
        if ($this->opts['countBytesTransfered']) $this->request['client']['bytesReceived'] += strlen($raw); // Log du traffic
        if (is_callable($callback)) $callback($raw);
        if (feof($this->socket)) break;
      }
    }
    $this->debug('Traffic Sent     : '.$this->request['client']['bytesSent']);
    $this->debug('Traffic Received : '.$this->request['client']['bytesReceived']);
    @fclose($this->socket);
  }
}
?>